from behave import *
from helper import generate_random_string, create_issuer_tenant, register_issuer_did, create_holder_tenant, create_verifier_tenant


@given(u'I can connect to OCM Engine')
def step_init(context):
    """
    step_impl Sets up initial variables for Base URLs
    """ 
    localhost = "http://localhost:"
    context.schema_manager_base_URL = localhost + "5101"
    context.connection_manager_base_URL = localhost + "5102"
    context.credential_manager_base_URL = localhost + "5103"
    context.proof_manager_base_URL = localhost + "5104"
    context.did_manager_base_URL = localhost + "5105"
    context.tenant_manager_base_URL = localhost + "5106"

    context.beta_schema_manager_base_URL = localhost + "5201"
    context.beta_connection_manager_base_URL = localhost + "5202"
    context.beta_credential_manager_base_URL = localhost + "5203"
    context.beta_proof_manager_base_URL = localhost + "5204"
    context.beta_did_manager_base_URL = localhost + "5205"
    context.beta_tenant_manager_base_URL = localhost + "5206"

    context.gamma_schema_manager_base_URL = localhost + "5301"
    context.gamma_connection_manager_base_URL = localhost + "5302"
    context.gamma_credential_manager_base_URL = localhost + "5303"
    context.gamma_proof_manager_base_URL = localhost + "5304"
    context.gamma_did_manager_base_URL = localhost + "5305"
    context.gamma_tenant_manager_base_URL = localhost + "5306"

    # Create 'issuer' tenant
    create_issuer_tenant_request = create_issuer_tenant(context.tenant_manager_base_URL)
    assert create_issuer_tenant_request.status_code == 201

    create_holder_tenant_request = create_holder_tenant(context.beta_tenant_manager_base_URL)
    assert create_holder_tenant_request.status_code == 201
    create_holder_2_tenant_request = create_holder_tenant(context.beta_tenant_manager_base_URL)

    create_verifier_tenant_request = create_verifier_tenant(context.gamma_tenant_manager_base_URL)
    assert create_verifier_tenant_request.status_code == 201

    print(create_issuer_tenant_request.json())
    context.issuer_id = create_issuer_tenant_request.json()['data']['id']
    context.holder_id = create_holder_tenant_request.json()['data']['id']
    context.holder_id_2 = create_holder_2_tenant_request.json()['data']['id']
    context.verifier_id = create_verifier_tenant_request.json()['data']['id']


@given(u'I register a DID for \'issuer\'')
def step_register_issuer_did(context):

    register_did_response = register_issuer_did(context.did_manager_base_URL, issuer_id=context.issuer_id)
    
    assert register_did_response.status_code == 201
    context.issuer_did = register_did_response.json()["data"][0]