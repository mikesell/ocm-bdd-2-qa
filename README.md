# ocm-bdd-2-qa



## Quick Install

Clone this repo, run the following commands

```
python3 -m venv venv
```
```
source venv/bin/activate
```
```
pip install -r requirements.txt
```

Ensure you have Python 3, pip and venv installed.

## Set Up Test Environment

Clone the OCM Engine Repo:

```
git clone --recurse-submodules https://gitlab.eclipse.org/tsabolov/ocm-engine/
```

### Pre-requisites
 - Docker

Check the postman collection from that repo:
`/ocm-engine/documentation/Gaia-X Organization Credential Manager.postman_collection.json`

and run the following commands:

```
cd ocm-engine
sudo ./scripts/start_instance.sh
sudo ./scripts/start_instance.sh
sudo ./scripts/start_instance.sh
cd ..
```

The above command is run three times to create a container for the holder, issuer, and verifier. 

### For troubleshooting

Run `sudo docker compose down -v --remove-orphans `

And also

```
sudo docker stop $(sudo docker ps -a -q)   
sudo docker rm $(sudo docker ps -a -q)
```

## Run Tests
If not within the python virtual env, run `source venv/bin/activate`

To execute tests, run 
```
behave features
```

## Save new packages

```
pip freeze > requirements.txt
```


## Generate test reports

For HTML report, run
```
behave -f html-pretty -o behave-report.html
```

For simple text report 
```
behave -f pretty -o report.txt
```
